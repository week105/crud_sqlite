import 'dog_dao.dart';
import 'dog.dart';

Future<void> addNewDog(Dog dog) {
  return DogDao.insertDog(dog);
}

Future<void> saveDog(Dog dog) {
  return DogDao.updateDog(dog);
}

Future<void> delDog(Dog dog) {
  return DogDao.deleteDog(0);
}

Future<List<Dog>> getDogs() {
  return DogDao.dogs();
}
